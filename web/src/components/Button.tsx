import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

interface ButtonProps {
  text: string;
  disabled?: boolean;
  onPress?: () => void;
}

export const Button = ({
  text,
  disabled = false,
  onPress = () => {},
}: ButtonProps) => (
  <TouchableOpacity onPress={onPress} disabled={disabled}>
    <View
      style={[
        {
          borderRadius: 40,
          height: 48,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#74E09A",
          opacity: disabled ? 0.5 : 1,
          width: 263,
        },
      ]}
    >
      <Text
        style={{
          fontWeight: "bold",
          fontSize: 15,
          color: "#FFFFFF",
        }}
      >
        {text}
      </Text>
    </View>
  </TouchableOpacity>
);
