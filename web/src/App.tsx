import React from "react";
import "./App.css";
import { Button } from "./components/Button";

const App = () => (
  <div className="App">
    <header className="App-header">
      <p>[HARDCODED] Hello, World! :D</p>
      <Button text={"Get text"} onPress={() => {}} />
    </header>
  </div>
);

export default App;
