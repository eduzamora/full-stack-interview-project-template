import express from "express";

const app = express();

app.get("/", (req, res) => {
  res.send("Hello, World! :D");
});

app.listen(3000, function () {
  console.log("App is listening on port 3000!");
});
